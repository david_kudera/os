source ~/.secrets

# Added by Toolbox App
export PATH="$PATH:/home/david/.local/share/JetBrains/Toolbox/scripts"

export EDITOR="nano"
export TOGGL_TRACKER_CONFIGURATION="/home/david/.config/toggltracker.json"
