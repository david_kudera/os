# OS

## Installation

**1. Install GPU drivers:**

* If AMD:

    ```bash
    $ sudo zypper install kernel-firmware-amdgpu
    ```

* If Nvidia:

    ```bash
    $ sudo zypper addrepo --refresh https://download.nvidia.com/opensuse/tumbleweed NVIDIA
    ```

    Open YaST software and install suggested packages.

*Restart*

**2. Enable RDP (optional):**

* Add `rdp` into firewall `public` zone
* Next enable RDP in Gnome settings

**3. Install git:**

```bash
$ sudo zypper install -y git
$ git config --global user.name "David Kudera"
$ git config --global user.email "kudera.d@gmail.com"
```

**4. Pull repository:**

```bash
$ mkdir -p ~/Projects
$ git clone https://gitlab.com/david_kudera/os.git ~/Projects/os
```

**5. Install packages:**

```bash
$ ~/Projects/os/scripts/packages.sh
```

* For full installation run also this:

    ```bash
    $ ~/Projects/os/scripts/packages_full.sh
    ```

**6. Install JetBrains apps (only with full installation):**

* Install JetBrains Toolbox `~/Downloads/jetbrains-toolbox`
* Install JetBrains Rider from JetBrains Toolbox
* Install JetBrains Webstorm from JetBrains Toolbox
* Install JetBrains DataGrip from JetBrains Toolbox
* Install JetBrains Fleet from JetBrains Toolbox

**7. Install Gnome extensions:**

* https://extensions.gnome.org/extension/615/appindicator-support/
* https://extensions.gnome.org/extension/1160/dash-to-panel/
* https://extensions.gnome.org/extension/3733/tiling-assistant/
* https://extensions.gnome.org/extension/1460/vitals/
* https://extensions.gnome.org/extension/5470/weather-oclock/
* https://extensions.gnome.org/extension/779/clipboard-indicator/
* https://extensions.gnome.org/extension/1262/bing-wallpaper-changer/
* https://extensions.gnome.org/extension/3193/blur-my-shell/
* https://gitlab.com/dkx/gnome/focused-window-monitor

*Restart*

**8. Create samba credentials:**

```bash
$ sudo nano /etc/samba/tower
$ sudo chmod 600 /etc/samba/tower
```

```
username=...
password=...
```

**9. Configure:**

```bash
$ ~/Projects/os/scripts/configure.sh
```

*Restart*

**10. Create .secrets:**

```bash
$ nano ~/.secrets
```

```
export CODESTATS_API_KEY="..."
```

**11. Configure zsh:**

```bash
$ ~/Projects/os/scripts/zsh.sh
```

*Restart*

**99. Finalize:**

* Sign in to 1Password
* Configure 1Password SSH Agent
* Configure 1Password SSH Key
* Configure Brave
* Set profile picture
* Set `Region & Language > Formats` to `Česko`

**100. Replace repository URL:**

```bash
$ git remote set-url origin git@gitlab.com:david_kudera/os.git
```

**101. Update locaate database:**

```bash
$ sudo updatedb
```
