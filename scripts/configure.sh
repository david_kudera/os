#!/usr/bin/env bash

set -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
ROOT_DIR="$SCRIPT_DIR/.."
CONFIG_DIR="$ROOT_DIR/config"

echo " -> Configuring desktop..."
gsettings set org.gnome.desktop.session        idle-delay            0
gsettings set org.gnome.desktop.wm.preferences button-layout         ":minimize,maximize,close"
gsettings set org.gnome.desktop.calendar       show-weekdate         true
gsettings set org.gnome.desktop.interface      clock-show-weekday    true
gsettings set org.gnome.desktop.interface      monospace-font-name   "MesloLGS NF 12"
# Disabled for now - breaks JetBrains IDEs keyboard layout switching
#gsettings set org.gnome.desktop.input-sources  per-window            true
gsettings set org.gnome.desktop.input-sources  sources               "[('xkb', 'cz+qwerty'), ('xkb', 'us')]"
gsettings set org.gnome.mutter                 dynamic-workspaces    true
gsettings set org.gnome.desktop.wm.keybindings switch-applications   "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-windows        "['<Alt>Tab']"
gsettings set org.gnome.desktop.wm.keybindings switch-group          "[]"
gsettings set org.gnome.shell.weather          automatic-location    true
gsettings set org.gnome.shell.weather          locations             "[<(uint32 2, <('Prague', 'LKKB', true, [(0.87478393392930087, 0.25383384995537522)], [(0.87411906122272931, 0.25249097093961048)])>)>]"
gsettings set org.gnome.Epiphany               default-search-engine "Google"
gsettings set org.gnome.shell                  favorite-apps         "['brave-browser.desktop', 'jetbrains-rider-b0a65063-b484-4a18-8964-43ef6ed39ea1.desktop', 'org.gnome.Console.desktop', 'org.gnome.Nautilus.desktop', 'app.ytmdesktop.ytmdesktop.desktop']"
gsettings set org.gnome.nautilus.preferences   show-image-thumbnails "always"
gsettings set org.gnome.shell.keybindings      toggle-message-tray   "[]"
#gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybindings.custom0 name    "Resources"
#gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybindings.custom0 command "/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=resources net.nokyan.Resources"
#gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybindings.custom0 binding "<Shift><Control>Escape"

echo " -> Configuring extensions..."
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas       set org.gnome.shell.extensions.dash-to-panel       click-action             "TOGGLE-SHOWPREVIEW"
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas       set org.gnome.shell.extensions.dash-to-panel       panel-element-positions  '{"0":[{"element":"leftBox","visible":true,"position":"stackedTL"},{"element":"taskbar","visible":true,"position":"stackedTL"},{"element":"dateMenu","visible":true,"position":"centerMonitor"},{"element":"rightBox","visible":true,"position":"stackedBR"},{"element":"systemMenu","visible":true,"position":"stackedBR"},{"element":"activitiesButton","visible":true,"position":"stackedBR"},{"element":"showAppsButton","visible":false,"position":"stackedTL"},{"element":"centerBox","visible":false,"position":"stackedBR"},{"element":"desktopButton","visible":false,"position":"stackedBR"}],"1":[{"element":"leftBox","visible":true,"position":"stackedTL"},{"element":"taskbar","visible":true,"position":"stackedTL"},{"element":"dateMenu","visible":true,"position":"centerMonitor"},{"element":"rightBox","visible":true,"position":"stackedBR"},{"element":"systemMenu","visible":true,"position":"stackedBR"},{"element":"activitiesButton","visible":true,"position":"stackedBR"},{"element":"showAppsButton","visible":false,"position":"stackedTL"},{"element":"centerBox","visible":false,"position":"stackedBR"},{"element":"desktopButton","visible":false,"position":"stackedBR"}]}'
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas       set org.gnome.shell.extensions.dash-to-panel       trans-use-custom-opacity true
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas       set org.gnome.shell.extensions.dash-to-panel       focus-highlight-dominant true
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas       set org.gnome.shell.extensions.dash-to-panel       focus-highlight-opacity  40
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas       set org.gnome.shell.extensions.dash-to-panel       multi-monitors           false
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas       set org.gnome.shell.extensions.dash-to-panel       hide-overview-on-startup true
gsettings --schemadir ~/.local/share/gnome-shell/extensions/appindicatorsupport\@rgcjonas.gmail.com/schemas set org.gnome.shell.extensions.appindicator        legacy-tray-enabled      false
gsettings --schemadir ~/.local/share/gnome-shell/extensions/Vitals\@CoreCoding.com/schemas                  set org.gnome.shell.extensions.vitals              hot-sensors              "['_processor_usage_', '_memory_usage_', '__network-rx_max__']"
gsettings --schemadir ~/.local/share/gnome-shell/extensions/Vitals\@CoreCoding.com/schemas                  set org.gnome.shell.extensions.vitals              menu-centered            true
gsettings --schemadir ~/.local/share/gnome-shell/extensions/Vitals\@CoreCoding.com/schemas                  set org.gnome.shell.extensions.vitals              position-in-panel        0
gsettings --schemadir ~/.local/share/gnome-shell/extensions/clipboard-indicator\@tudmotu.com/schemas        set org.gnome.shell.extensions.clipboard-indicator toggle-menu              "['<Super>v']"
gsettings --schemadir ~/.local/share/gnome-shell/extensions/blur-my-shell\@aunetx/schemas                   set org.gnome.shell.extensions.blur-my-shell.panel blur                     false

echo " -> Configuring user..."
if [ "$SHELL" != "$(which zsh)" ]; then
    chsh -s $(which zsh)
fi

echo " -> Adding config files..."
ln -s -f /usr/share/applications/1password.desktop ~/.config/autostart/
if [ -f /var/lib/flatpak/exports/share/applications/org.ferdium.Ferdium.desktop ]; then
    ln -s -f /var/lib/flatpak/exports/share/applications/org.ferdium.Ferdium.desktop ~/.config/autostart/
fi
if [ -d /var/lib/flatpak/app/com.github.IsmaelMartinez.teams_for_linux ]; then
    cp -f "$CONFIG_DIR/autostart/teams-for-linux.desktop" ~/.config/autostart/
fi
if [ -d ~/Apps/TogglTracker ]; then
    cp -f "$CONFIG_DIR/autostart/toggltracker.desktop" ~/.config/autostart/
fi
cp -f "$CONFIG_DIR/bookmarks" ~/.config/gtk-3.0/bookmarks

if [ ! -d /mnt/tower_david ]; then
    echo "Configuring mount tower_david"
    sudo mkdir /mnt/tower_david
    echo "//192.168.88.43/david /mnt/tower_david cifs credentials=/etc/samba/tower,noauto,x-systemd.automount,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,uid=$UID,gid=$GID 0 0" | sudo tee -a /etc/fstab
fi

echo " -> Removing unnecessary files..."
if [ -f ~/.local/share/applications/jetbrains-fleet.desktop ]; then
    mv -f ~/.local/share/applications/jetbrains-fleet.desktop ~/.local/share/applications/jetbrains-fleet._bak_desktop
fi
