#!/usr/bin/env bash

set -e

add_repo_if_not_exists() {
    local test_alias="$1"
    local repo_url="$2"
    local repo_name="${3:-}"

    if ! zypper --terse lr | grep -qw "$test_alias"; then
            if [ -n "$repo_name" ]; then
                sudo zypper addrepo "$repo_url" "$repo_name"
            else
                sudo zypper addrepo "$repo_url"
            fi
    fi
}

mkdir -p ~/Apps

echo " -> Installing dependencies..."
sudo zypper install -y curl libicu

echo " -> Adding repositories..."
sudo rpm --import https://downloads.1password.com/linux/keys/1password.asc
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
add_repo_if_not_exists 1password https://downloads.1password.com/linux/rpm/stable/x86_64 1password
add_repo_if_not_exists brave-browser https://brave-browser-rpm-release.s3.brave.com/brave-browser.repo
sudo zypper --gpg-auto-import-keys refresh | echo 'a'
sudo zypper refresh | echo 'a'

echo " -> Installing packages..."
sudo zypper install -y 1password brave-browser gnome-tweaks unzip gnome-console zsh remmina

echo " -> Installing flatpak packages..."
sudo flatpak install -y \
    com.system76.Popsicle

if [ ! -d ~/.local/share/fonts/meslo ]; then
    echo " -> Installing Meslo fonts..."
    mkdir -p ~/.local/share/fonts/meslo
    curl -fsSL "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf" -o ~/.local/share/fonts/meslo/MesloLGS\ NF\ Regular.ttf
    curl -fsSL "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf" -o ~/.local/share/fonts/meslo/MesloLGS\ NF\ Bold.ttf
    curl -fsSL "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf" -o ~/.local/share/fonts/meslo/MesloLGS\ NF\ Italic.ttf
    curl -fsSL "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf" -o ~/.local/share/fonts/meslo/MesloLGS\ NF\ Bold\ Italic.ttf
    fc-cache -f -v
fi
