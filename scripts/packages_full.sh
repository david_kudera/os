#!/usr/bin/env bash

set -e

JETBRAINS_TOOLBOX_VERSION="2.1.3.18901"
NODE_VERSION="20"

add_repo_if_not_exists() {
    local test_alias="$1"
    local repo_url="$2"
    local repo_name="${3:-}"

    if ! zypper --terse lr | grep -qw "$test_alias"; then
            if [ -n "$repo_name" ]; then
                sudo zypper addrepo "$repo_url" "$repo_name"
            else
                sudo zypper addrepo "$repo_url"
            fi
    fi
}

mkdir -p ~/Apps

echo " -> Installing dependencies..."
sudo zypper install -y curl libicu

echo " -> Adding repositories..."
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
add_repo_if_not_exists graphics https://download.opensuse.org/repositories/graphics/openSUSE_Tumbleweed/graphics.repo
add_repo_if_not_exists vscode https://packages.microsoft.com/yumrepos/vscode
add_repo_if_not_exists snappy https://download.opensuse.org/repositories/system:/snappy/openSUSE_Tumbleweed
add_repo_if_not_exists packages-microsoft-com-prod https://packages.microsoft.com/config/opensuse/15/prod.repo
sudo zypper --gpg-auto-import-keys refresh | echo 'a'
sudo zypper refresh | echo 'a'

echo " -> Configuring snap..."
sudo zypper dup --from snappy
sudo zypper install -y snapd
sudo systemctl enable --now snapd
sudo systemctl enable --now snapd.apparmor

echo " -> Installing packages..."
sudo zypper install -y btop d-feet mc bat gimp inkscape jq epiphany "nodejs$NODE_VERSION" docker docker-compose docker-compose-switch waypipe kitty hyprland code menulibre cifs-utils dotnet-sdk-8.0 findutils-locate postgresql
sudo systemctl enable docker
sudo usermod -G docker -a $USER

echo " -> Installing flatpak packages..."
sudo flatpak install -y \
    flathub org.ferdium.Ferdium \
    com.github.IsmaelMartinez.teams_for_linux \
    app.ytmdesktop.ytmdesktop

echo " -> Installing snap packages..."
sudo snap install winbox

if [ ! -f ~/.bun/bin/bun ]; then
    echo " -> Install bun..."
    curl -fsSL https://bun.sh/install | bash
fi

if [ ! -d ~/.local/share/JetBrains/Toolbox ]; then
    echo " -> Downloading JetBrains Toolbox..."
    curl -fsSL "https://download.jetbrains.com/toolbox/jetbrains-toolbox-${JETBRAINS_TOOLBOX_VERSION}.tar.gz" -o ~/Downloads/jetbrains-toolbox.tar.gz
    tar -xzvf ~/Downloads/jetbrains-toolbox.tar.gz -C ~/Downloads
    mv ~/Downloads/jetbrains-toolbox-2.1.3.18901/jetbrains-toolbox ~/Downloads/
    rm ~/Downloads/jetbrains-toolbox.tar.gz
    rm -r ~/Downloads/jetbrains-toolbox-2.1.3.18901
fi

if [ ! -d ~/Apps/TogglTracker ]; then
    curl -fsSL "https://gitlab.com/dkx/apps/toggl-tracker/-/jobs/5862353906/artifacts/raw/out/make/zip/linux/x64/toggltracker-linux-x64-0.0.6.zip" -o /tmp/toggltracker.zip
    unzip /tmp/toggltracker.zip -d ~/Apps
    mv ~/Apps/toggltracker-linux-x64 ~/Apps/TogglTracker
    rm /tmp/toggltracker.zip
fi
