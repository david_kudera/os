#!/usr/bin/env bash

set -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
ROOT_DIR="$SCRIPT_DIR/.."
CONFIG_DIR="$ROOT_DIR/config"

if [ ! -d ~/.oh-my-zsh ]; then
    echo " -> Installing OhMyZsh..."
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

if [ ! -d ~/.oh-my-zsh/custom/themes/powerlevel10k ]; then
    echo " -> Installing powerlevel10k..."
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/custom/themes/powerlevel10k
fi

if [ ! -d ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions ]; then
    echo " -> Installing autosuggestions..."
    git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
fi

if [ ! -d ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting ]; then
    echo " -> Installing highlighting..."
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
fi

if [ ! -d ~/.oh-my-zsh/custom/plugins/codestats ]; then
    echo " -> Installing codestats..."
    git clone git@gitlab.com:code-stats/code-stats-zsh.git ~/.oh-my-zsh/custom/plugins/codestats
fi

echo " -> Adding config files..."
cp -f "$CONFIG_DIR/.zshenv" ~/.zshenv
cp -f "$CONFIG_DIR/.zshrc" ~/.zshrc
cp -f "$CONFIG_DIR/.p10k.zsh" ~/.p10k.zsh
